#!/bin/bash
set -e

# Install ssh-agent if not already installed
which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
