<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IndexRouteTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testOnline()
    {
        $response = $this->get('/user/prova');

        $response->assertStatus(200);
        $response->assertSeeText("prova visited");
        $this->assertDatabaseHas("users", ["name" => "prova"]);
    }
}
