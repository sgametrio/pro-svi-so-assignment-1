<!doctype html>
<html>
   <head>
      <title>{{ $user->name }} profile</title>
      <style>
         .center {
            height: 100%;
            width: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
         }
         .big {
            font-size: 2.25rem;
         }
      </style>
   </head>
   <body style="height:100vh; margin: 0">
      <div class="center">
         <div class="big">{{ $user->name }} visited this page {{ $user->visits }} times</div>
      </div>
   </body>
</html>