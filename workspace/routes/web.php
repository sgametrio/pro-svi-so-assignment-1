<?php

use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // Return view to insert the username and redirects there
    return view("welcome");
});

Route::get('/user/{username}', function ($username) {
    $user = User::where("name", $username)->first();
    if (is_null($user)) {
        $user = new User();
        $user->name = $username;
    }
    $user->visits = $user->visits + 1;
    $user->save();
    return view('user', ["user" => $user]);
});

Route::get('/metrics', function () {
    $total_visits = User::all()->sum('visits');
    return "laravel_total_user_visits ".$total_visits;
});