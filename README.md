# Assignment 1 - DevOps

### Students and repository

* Carrara Demetrio - 807894

* Ribaudo Francesco - 807847

Repository is available at: https://gitlab.com/sgametrio/pro-svi-so-assignment-1 and a demo is online at http://90.147.188.242

### Environment tool

* Containerization/Virtualization: __Docker__ and __Docker Compose__
* Configuration: __Ansible__
* CI/CD: __GitLab CI/CD__
* Monitoring: __Prometheus__

### Project explanation

We want to develop a Laravel application that connects to a MySQL database to save/retrieve data about users. In short, we want to display a sentence every time someone access our website at `/user/{username}`: "Hey `username`, you visited this page `n` times".

On the DevOps side, we configure the application for deployment with Ansible, manage services and containers with the help of Docker (and Docker Compose), test it with the integrated platform on GitLab and monitor it with Prometheus and cAdvisor.

#### Docker containers and Docker Compose services

Docker and Docker Compose helps keeping logical components divided. We configured 5 containers as Docker Compose services: 

* `webserver`: runs `nginx` on port 80 and contains webserver logic and configuration
* `workspace`: contains `php` executables, laravel application and let's you use `bash` and `php artisan` utility to make migrations and code generations. To prepare this container to serve a laravel application we had to install some custom PHP libraries and composer (package manager for PHP)
* `database`: runs `mysql` on port 3306 and contains database logic and configuration such as database and user initialization
* `cadvisor`: monitors containers physical resources usage and makes a bridge between containers and monitor/alerting system
* `prometheus`: monitors metrics scraped on Laravel application and cAdvisor

These containers needs only little configuration because they already have everything included. 
But we want to:

* Tell NGINX to use a separate PHP interpreter (which listen to the port 9000)
* Tell MySQL to create user and database during the first build so we have separated data, roles and accesses: we can make this by setting environment variables because default MySQL container has already the logic to do this
* Share same volume (data) between `workspace` and `webserver`, on the development side, allowing us to see immediately our source code changes reflected on the `webserver`
  `database` has its own volume to keep data safe
* Tell Prometheus to monitor cAdvisor and Laravel app on their respective container ports

Docker Compose abstracts these configurations and make it very easy to let containers see each others on the network with their name.

Docker Compose use `.env` file to pass environment variables to container that needs secrets (`database` for example) and to set up shared variables like `MYSQL_USER` or `MYSQL_ROOT_PASSWORD`

#### GitLab CI/CD

We plan to add tests that run automatically every push to see if it has broken something important. 
Tests are like: 

* check if we can read something from db (so we can check db integration with the webapp)
* visits and check user visits
* visits metrics endpoint to see if it is up

We use 2 different branches in the codebase because we want to differentiate something that is "sort of" stable and something in very development: `develop` branch will be used to test new features and when we merge `develop` into `master` branch the auto-deploy can start to push to production or staging.

Automated tasks involve 3 different and consequential jobs:
* __build__: Builds containers using `docker-compose`
* __test__: Tests webapp source code with `phpunit` 
* __deploy__: Deploys containers to a remote service (`Cloud Garr`)

#### Ansible

Ansible automates task regarding deployment and virtual machine configuration. 
We configured it so that it connects to a machine on Cloud GARR through SSH. This involves copying public SSH key to target machine and keeping private one as a project environment variable used by GitLab runner.

With Ansible we:

1. Update target packages
2. Install dependencies needed for running `docker` and `docker-compose`
3. Clone repository
4. Build and run containers using correct secrets

#### Prometheus and cAdvisor

With the help of Prometheus and cAdvisor we can monitor container status, get notified when something has broken and do some analytics on data coming from the system being used. 

cAdvisor gives us metrics on resources used by containers: disk, memory and cpu, so we can set alerts when these metrics become crazy.

On the webapp we keep track, for example, of the total number of user visits and we expose it on `/metrics` endpoint so Prometheus can scrape it and show it on its dashboard. We could add more metrics other than that, but for the simple app we are showing, it is enough.

### Future works

* Add more metrics and alerts to be sure that everything in staging environment is running as it should
* Add production environment and integration tests on staging one
* Deploy with Kubernetes or something similar to multiple target hosts
* Integrate Ansible with OpenStack to create automatically all the needed virtual machines