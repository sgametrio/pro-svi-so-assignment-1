#!/bin/bash
# set variables to exit after first error
set -e 

# Run ssh-agent (inside the build environment)
eval $(ssh-agent -s)

# Add the SSH key stored in SSH_PRIVATE_GARR_KEY variable to the agent store
echo "$SSH_PRIVATE_GARR_KEY_DEPLOY" | tr -d '\r' | ssh-add - > /dev/null

# Create the SSH directory and give it the right permissions
mkdir -p ~/.ssh
chmod 700 ~/.ssh

# Add SSH_KNOWN_HOSTS
echo "$SSH_DEPLOY_KNOWN_HOSTS" > ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

# Automatic deploy DEPLOY_HOST using ssh with Ansible 
ansible-playbook -i ansible/inventory ansible/playbook.yml --vault-password-file=.vault-pass.sh