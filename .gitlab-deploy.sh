#!/bin/bash
# set variables to exit after first error
set -e 

# Run ssh-agent (inside the build environment)
eval $(ssh-agent -s)

# Add the SSH key stored in SSH_PRIVATE_GARR_KEY variable to the agent store
echo "$SSH_PRIVATE_GARR_KEY" | tr -d '\r' | ssh-add - > /dev/null

# Create the SSH directory and give it the right permissions
mkdir -p ~/.ssh
chmod 700 ~/.ssh

# Add SSH_KNOWN_HOSTS
echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

# Deploy DEPLOY_HOST using ssh
# Change docker-compose build to deploy
# Need to check if all variables in .env are set otherwise we have runtime errors

ssh ubuntu@$DEPLOY_HOST "cd ~/dev-ops \
&& git stash \
&& git pull origin master \
&& docker-compose -f docker-compose.yml -f docker-compose.prod.yml build \
&& docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d \
&& docker-compose exec -T workspace composer install --prefer-dist --no-progress --no-suggest --no-interaction \
&& docker-compose exec -T workspace php artisan key:generate \
&& echo 'maybe fill .env files with GitLab variables and deploy (docker-compose up/deploy).' "
echo "Deploy to $DEPLOY_HOST completed!"
