#!/bin/bash
# set variables to exit after first error
set -xe 

DC_WORKSPACE=("docker-compose" "exec" "-T" "workspace")

"${DC_WORKSPACE[@]}" composer install --prefer-dist --no-progress --no-suggest --no-interaction
"${DC_WORKSPACE[@]}" cp .env.example .env
"${DC_WORKSPACE[@]}" php artisan key:generate
